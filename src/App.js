import React, { Component } from 'react';
import './App.css';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';

// Routing imports
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Home from './pages/Home'
import Clusters from './pages/Clusters'
import HealthMon from './pages/HealthMon'

class App extends Component {
    render() {
	return(
        <div>
            <Router>
                <AppBar position="static">
            <Toolbar>
              <IconButton edge="start" className="menu-button" color="inherit" aria-label="menu">
                <MenuIcon />
              </IconButton>
              <Typography variant="h5" className="a-title">
                DBAdmin
              </Typography>
              <Button className="nav-button">
                  <Link to="/">Home</Link>
              </Button>
              <Button className="nav-button">
                  <Link to="/healthmon">Health Monitor</Link>
              </Button>
            </Toolbar>
          </AppBar>

            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/healthmon">
                <Clusters />
              </Route>
              <Route exact path="/healthmon/:id">
                <Clusters />
              </Route>
              <Route path="/healthmon/:id/:nodeid">
                <HealthMon />
              </Route>

            </Switch>
        </Router>



	    </div>
	);
    }
}

export default App;
