import React, { Component } from 'react'
import './HealthMon.css'
import Disk from './components/Disk'
import Cpu from './components/Cpu'
import Throughput from './components/Throughput'
import Details from './components/Details'

class VisTool extends Component {

    constructor(props) {
        var parts = window.location.href.split("/");
        super(props);
        this.state = {
            cluster: parts[parts.length - 2],
            node: parts[parts.length - 1]
        };
    }

    render() {

        console.log(this.state.cluster + "/" + this.state.node)
        return (
            <div class="vis-container">
                <div class="util-container">
                    <Details />
                    <Disk />
                    <Cpu />
                </div>
                <div class="throughput-container">
                    <Throughput />
                </div>
            </div>
        );
    }
}

export default VisTool;
