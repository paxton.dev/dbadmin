import React, { Component } from 'react'
import './Clusters.css'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Nodes from './components/Nodes'
import HealthMon from './HealthMon'

// Routing imports
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

class FetchClusters extends Component {

    // Construct the state object, passing in paramaters (props)
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            response: []
        };
    }

    // Fetch the disk utilisation
    componentDidMount() {
        fetch("http://soc-web-liv-56.napier.ac.uk:80/api/clusters")
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    loading: false,
                    response: data.response
                });
            })
    }

    render() {

        if (this.state.loading) {
            return (
                <div class="loading">
                    <CircularProgress />
                </div>
            )
          }

        return (

            <Router>

                <div class="clusters-container">
                    {this.state.response.map((cluster)=>
                        <Button className="cluster-button" id={cluster}>
                            <Link to={"/healthmon/" + cluster}>{cluster}</Link>
                        </Button>
                    )}
                </div>

                <Switch>
                    <Route exact path="/healthmon/:id/">
                        <Nodes />
                    </Route>

                    <Route exact path="/healthmon/:id/:nodeid">
                        <HealthMon />
                    </Route>
                </Switch>

            </Router>
        );
    }
}

export default FetchClusters;
