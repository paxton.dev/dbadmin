import React, { Component } from 'react';

class TestFetch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            response: []
        };
    }

    componentDidMount() {
    fetch("http://soc-web-liv-56.napier.ac.uk:80/api/home")
        .then(res => res.json())
        .then(
            (data) => {
                this.setState({
                    response: data
                });
                console.log(this.state.response);
            }
        )
    }

    render() {
        return (
	    <div>
            <p>JSON Response:</p>
	        
	    </div>
        );
    }
}

export default TestFetch;
