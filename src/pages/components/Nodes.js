import React, { Component } from 'react';
import './Nodes.css'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'

// Routing imports
import {
  Link
} from "react-router-dom";

class FetchNodes extends Component {

    // Construct the state object, passing in paramaters (props)
    constructor(props) {
        var parts = window.location.href.split("/");
        super(props);
        this.state = {
            loading: true,
            response: [],
            cluster: parts[parts.length - 1]
        };
    }

    // Fetch the cpu utilisation
    componentDidMount() {

        fetch("http://soc-web-liv-56.napier.ac.uk:80/api/cluster/" + this.state.cluster)
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    loading: false,
                    response: data.response
                });
            })
    }

    // Component to render
    render() {

        if (this.state.loading) {
            return (
                <div class="loading">
                    <CircularProgress />
                </div>
            )
          }

        // Return the component
        return (
            <div class="node-container">

            {this.state.response.map((cluster)=>
                <Button className="node-button">
                    <Link to={"/healthmon/" + this.state.cluster + "/" + Object.keys(cluster)[0]}>{Object.keys(cluster)[0]}</Link>
                </Button>
            )}

            </div>
        );
    }
}

export default FetchNodes;
