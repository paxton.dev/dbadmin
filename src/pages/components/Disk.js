import React, { Component } from 'react';
import Highcharts from "highcharts/highcharts.js";
import highchartsMore from "highcharts/highcharts-more.js";
import solidGauge from "highcharts/modules/solid-gauge.js";
import HighchartsReact from "highcharts-react-official";
import './Disk.css'

highchartsMore(Highcharts);
solidGauge(Highcharts);

class FetchDisk extends Component {

    // Construct the state object, passing in paramaters (props)
    constructor(props) {
        super(props);
        this.state = {
            response: []
        };
    }

    // Fetch the disk utilisation
    componentDidMount() {
        fetch("http://soc-web-liv-56.napier.ac.uk:80/api/util/disk")
            .then(res => res.json())
            .then((data) => {
                this.setState({response: data.response});
                console.log(this.state.response);
            })
    }

    // Component to render
    render() {

        const options = {
            chart: {
                type: "solidgauge"
            },
            title: null,
            align: "left",
            pane: {
                size: '50%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            exporting: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            // the value axis
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                min: 0,
                max: 100,
                lineWidth: 0,
                tickWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    y: -70,
                    text: "Disk Utilisation"
                },
                labels: {
                    y: 16
                }
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: -25,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            },
            series: [
                {
                    data: [this.state.response],
                    dataLabels: {
                      format: '<div class="dataLabel" style="text-align:center">' +
                              '<span style="font-size:15px">{y}%</span><br/>' +
                              '</div>'
                    }
                }
            ]
        };


        // Return the component
        return (
            <div class="disk-container">
                <HighchartsReact highcharts={Highcharts} options={options} />
            </div>
        );
    }
}

export default FetchDisk;
