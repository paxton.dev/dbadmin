import React, { Component } from 'react';
import './Throughput.css'
import Highcharts from 'highcharts/highstock';
import HighchartsReact from 'highcharts-react-official';

class FetchThroughput extends Component {

    // Construct the state object, passing in paramaters (props)
    constructor(props) {
        var parts = window.location.href.split("/");
        super(props);
        this.state = {
            cluster: parts[parts.length - 2],
            node: parts[parts.length - 1],
            response: []
        };
    }

    // Fetch the cpu utilisation
    componentDidMount() {
        fetch("http://soc-web-liv-56.napier.ac.uk:80/api/util/throughput")
            .then(res => res.json())
            .then((data) => {
                this.setState({response: data.response});
                console.log(this.state.response);
            })
    }

    // Component to render
    render() {

        var node = this.state.node
        var cluster = this.state.cluster

        const options = {
            title: {
                text: "Cluster " + cluster + " - " + node + " Throughput"
            },
            yAxis: {
                title: {
                    text: 'Throughput (MB/S)'
                }
            },
            xAxis: {
                title: {
                    text: 'Time (Last 24 hours)'
                }
            },
            series: [{
                name: "Throughput (Write)",
                data: [Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1]
            },
            {
                name: "Throughput (Read)",
                data: [Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1, Math.floor(Math.random() * 30) + 1]
            }]
        };


        // Return the component
        return (
            <div class="throughput-container">
            <HighchartsReact highcharts={Highcharts} options={options} />
            </div>
        );
    }
}

export default FetchThroughput;
