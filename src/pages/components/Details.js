import React, { Component } from 'react';
import Highcharts from "highcharts/highcharts.js";
import highchartsMore from "highcharts/highcharts-more.js";
import solidGauge from "highcharts/modules/solid-gauge.js";
import './Details.css'

highchartsMore(Highcharts);
solidGauge(Highcharts);

class FetchDisk extends Component {

    constructor(props) {
        var parts = window.location.href.split("/");
        super(props);
        this.state = {
            response: [],
            cluster: parts[parts.length - 2],
            node: parts[parts.length - 1]
        };
    }

    // Fetch the disk utilisation
    componentDidMount() {
        fetch("http://soc-web-liv-56.napier.ac.uk:80/api/cluster/" + this.state.cluster)
            .then(res => res.json())
            .then((data) => {
                this.setState({response: data.response});
            })
    }

    // Component to render
    render() {

        var node = this.state.node;
        var latency = 0;
        var state = "";
        var status = "";
        var address = "";
        var load = 0;
        var tokens = 0;
        var owns = 0;

        for (var i = 0; i < this.state.response.length; i++) {
            var key = Object.keys(this.state.response[i])[0]
            if (node === key) {
                latency = this.state.response[i][key]["latency"];
                state = this.state.response[i][key]["state"];
                status = this.state.response[i][key]["status"];
                address = this.state.response[i][key]["address"];
                load = this.state.response[i][key]["load"];
                tokens = this.state.response[i][key]["tokens"];
                owns = this.state.response[i][key]["owns"];
                break;
            }
        }

        // Return the component
        return (
            <div class="node-details">
                <h3>  {this.state.cluster}{this.state.node} Details </h3>
                <p>Status - {state}</p>
                <p>Latency - {latency}ms</p>
                <p>State - {status}</p>
                <p>Address - {address}</p>
                <p>Load - {load}mb/s</p>
                <p>Tokens - {tokens}</p>
                <p>Owns - {owns}%</p>
            </div>
        );
    }
}

export default FetchDisk;
